#!/usr/bin/env nodejs
// =================================================================
// get the packages we need ========================================
// =================================================================
let fs              = require('fs');
let http            = require('http');
let https           = require('https');

let key             = fs.readFileSync('./sslcert/privkey.pem', 'utf8');
let cert            = fs.readFileSync('./sslcert/fullchain.pem', 'utf8');
let ca              = fs.readFileSync('./sslcert/chain.pem', 'utf8');
let credentials     = {key: key, cert: cert, ca: ca};

let express 	    = require('express');
let bodyParser      = require('body-parser');
let morgan          = require('morgan');
let mongoose        = require('mongoose');
//let swaggerUi       = require('swagger-ui-express');

let config          = require('./config');       // get our config file
// let swaggerDocument = require('./swagger.json'); //Swagger doc file

let app             = express();

let httpsServer     = https.createServer(credentials, app);
let httpServer     = http.createServer(function (req, res) {
    res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
});

// =================================================================
// configuration ===================================================
// =================================================================

mongoose.connect(config.database); // connect to database

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// use morgan to log requests to the console
app.use(morgan('dev'));

app.use('/api', require('./app/routes/tools'));
app.use('/api', require('./app/routes/interestPoint'));
app.use('/api', require('./app/routes/users'));
app.use('/api', require('./app/routes/success'));
app.use('/api', require('./app/routes/event'));
app.use('/api', require('./app/routes/userHasSuccess'));
app.use('/api', require('./app/routes/partners'));
app.use('/api', require('./app/routes/rewards'));
app.use('/api', require('./app/routes/answers'));
app.use('/api', require('./app/routes/questions'));

app.get('/',function(req,res){
	res.send("Hello there !");
});

httpServer.listen(8080);
console.log('API redirects from http to https');
httpsServer.listen(8443);
console.log('API run at https://localhost:' + 8443);
