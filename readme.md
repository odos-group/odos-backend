# ODOS Backend

	Projet intensif 2018 DOME

# Description

	L'Api est développée sur un serveur NodeJS avec un module Express pour la gestion des requêtes.
	Les données sont sauvegardées dans une base orientée document MongoDB.
	Le lien avec la base est réalisé par le module Mongoose.
	La description des routes est dans le fichier api.pdf à la racine du projet.

	L'Api est disponible à l'URL suivante : https://dome7.ensicaen.fr/api/

	Le serveur est initialisé sur le port 8443, la redirection est faites par nginx lorsque le path commence par /api

	Le site web est disponible à l'URL suivante : https://dome7.ensicaen.fr/

	La base mongoDB est lancée sur l'adresse 127.0.0.1:27017

	Le package certbot a été utilisé pour générer les certificats via letsencrypt.
	Les commandes sont dans le fichier commandeCertificats.
	Les certificats sont valides 3 mois.

# Utilisation

	Le fichier principal du serveur est /home/administrateur/backend/odos-backend/server.js
	Démarrage : forever start server.js
	Arret : forever stopall

	//Voir le contenu de la base depuis une console
	mongo
	use test
	show collections
	//voir le contenu d'une collection
	db.collections.find()
	//vider une collection
	db.collections.drop()

# Installation

	//NodeJS
	curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
	sudo apt-get install -y nodejs

	//MongoDB Ubuntu 16.04 uniquement
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
	echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
	sudo apt-get update
	sudo apt-get install -y mongodb-org
	sudo service mongod start

	//Clonage du git
	git clone https://gitlab.com/odos-group/odos-backend.git

	//Initialisation
	npm install
	forever start server.js

# Technologies 

	NodeJS - Javascript
	MongoDB

#License

Creative Commons

#Authors

Alexis | LoiK

