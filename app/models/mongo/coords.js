let mongoose = require('mongoose');

let Schema = mongoose.Schema({
    lat      :  Number,
    lng      :  Number
});

module.exports = mongoose.model('Coords',Schema);
