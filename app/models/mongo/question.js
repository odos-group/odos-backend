let mongoose = require('mongoose');


// create scheme for the database and the app
let Schema = mongoose.Schema({
		  
	label       : String,
	value 		: Number,
	interestPointId: String,
	answerId	   :String

    
});

module.exports = mongoose.model('Question', Schema);