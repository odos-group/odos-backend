let mongoose = require('mongoose');
let Coords = require('./coords');

let Schema = mongoose.Schema({
    description : String,
    value : Number,
    type : String,
    location :{
        type: {type : String},
        coordinates: [Number]
    }
    
});

module.exports = mongoose.model('Event',Schema);
