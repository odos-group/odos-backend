let mongoose = require('mongoose');
let Coords = require('./coords');

// create scheme for the database and the app
let Schema = mongoose.Schema({
		  
	name       : String,
	website    : String,
	location   :{
        type: {type : String},
        coordinates: [Number]
    }

    
});

module.exports = mongoose.model('Partner', Schema);