let mongoose = require('mongoose');

// create scheme for the database and the app
let Schema = mongoose.Schema({
		  
	userId       : String,
	successId    : String,
    
});

module.exports = mongoose.model('UserHasSuccess', Schema);