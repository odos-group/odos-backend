let mongoose = require('mongoose');
let bcrypt   = require('bcrypt-nodejs');

// create scheme for the database and the app
let Schema = mongoose.Schema({
		  
	email        : String,
	password     : String,
	score        : Number,
	nickname	 : String,
	avatar		 : [Number],
	distance	 : Number,
	pet			 : Number,
	petUnlock	 : [Number]

    
});

// generating a hash
Schema.statics.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
Schema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};



// create the model for users and expose it
module.exports = mongoose.model('User', Schema);