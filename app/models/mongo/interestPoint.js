let mongoose = require('mongoose');
let Coords = require('./coords');

let Schema = mongoose.Schema({
    name : String,
    description : String,
    value : Number,
    pictureURL : String,    
    websiteURL : String,
    location :{
        type: {type : String},
        coordinates: [Number]
    }
    
});

module.exports = mongoose.model('InterestPoint',Schema);
