let mongoose = require('mongoose');

let Schema = mongoose.Schema({
    userId	 :  String,
    TrajetId :  String,
    locations :[
    	location :{
        	type: {type : String},
        	coordinates: [Number]
    	}
	]
});

module.exports = mongoose.model('Coords',Schema);
