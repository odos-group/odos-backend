let mongoose = require('mongoose');

let Schema = mongoose.Schema({
    
    description : String,
    price:Number,
    partnerId:String
});

module.exports = mongoose.model('Reward',Schema);
