let express = require('express');
let jwt     = require('jsonwebtoken'); // used to create, sign, and verify tokens

let router = express.Router();

let Answer   = require('../models/mongo/answer'); // get our mongoose
let config = require('../../config'); // get our config file

let secret = config.secret;

router.get('/answer', function(req, res) {
    Answer.find({}, function(err, answer) {
        res.json(answer);
    });
});


module.exports = router;