let express = require('express');
let jwt     = require('jsonwebtoken'); // used to create, sign, and verify tokens

let router = express.Router();

let Success   = require('../models/mongo/success'); // get our mongoose
let config = require('../../config'); // get our config file

let secret = config.secret;

router.get('/success', function(req, res) {
    Success.find({}, function(err, success) {
        res.json(success);
    });
});

//add success
router.post('/addsuccess',function(req,res){
    Success.findOne({
        name: req.body.name
    }, function(err, success) {

        if (err) throw err;

        if (!success) {
            // create a new success
            let NewSuccess = new Success({
                name: req.body.name,
                description: req.body.description,
                image: req.body.image
            });
            // Save it in the database
            NewSuccess.save(function(err) {
                if (err) throw err;

                console.log('Success saved successfully');
                res.json({ success: true, message: 'add success'});
            });

        } else if (success) {

            res.json({ success: false, message: 'add success failed. success already exist' });
        }

    });


});

module.exports=router;