let express = require('express');
let jwt     = require('jsonwebtoken'); // used to create, sign, and verify tokens

let router = express.Router();

let User   = require('../models/mongo/user'); // get our mongoose
let config = require('../../config'); // get our config file

let secret = config.secret;

// ---------------------------------------------------------
// authentication (no middleware necessary since this isnt authenticated)
// ---------------------------------------------------------
// http://localhost:8080/api/authenticate

router.post('/authenticate', function(req, res) {
    // find the user
    User.findOne({
        email: req.body.email
    }, function(err, user) {

        if (err) throw err;

        if (!user) {
            res.json({ success: false, message: 'Echec de l\'authentification : utilisateur inexistant'});
        } else if (user) {

            // check if password matches
            if (!user.validPassword(req.body.password)) {
                res.json({ success: false, message:  'Echec de l\'authentification : mot de passe erroné'});
            } else {

                // if user is found and password is right
                // create a token
                let payload = {
                    admin: user.admin
                };
                let token = jwt.sign(payload, secret, {
                    expiresIn: 86400 // expires in 24 hours
                });

                res.json({
                    success: true,
                    token: token,
                    message: 'Authentification réussie'
                });
            } 

        }

    });
});

router.post('/register',function(req,res){
    User.findOne({
        email: req.body.email
    }, function(err, user) {

        if (err) throw err;

        if (!user) {
            // create a new user
            console.log(req.body.password);
            let NewUser = new User({
                email: req.body.email,
                password: User.generateHash(req.body.password),
                nickname: req.body.nickname,
                score: 300,
                avatar: [0,0,0],
                distance : 0,
                pet : 0,
                petUnlock : [1,0,0,0,0,0,0,0,0,0],
                admin: false
            });
            // Save it in the database
            NewUser.save(function(err) {
                if (err) throw err;

                console.log('User saved successfully');
                res.json({ success: true, message: 'Enregistrement réussi'});
            });

        } else if (user) {

            res.json({ success: false, message: 'Utilisateur déjà inscrit' });
        }

    });
});

router.post('/user',function(req,res){
    User.findOne({
        email: req.body.email
    },{password:0,__v:0}, function(err, user){
        if(!user){
            res.json({ success: false, message: 'L\'utilisateur n\'existe pas' });
        }else{
         res.json(user);
        }
    });
});

// Params : email:String | pseudo:String
router.post('/nickname', function(req,res){
    User.findOne({
        email: req.body.email
    }, function(err, user){

        if (err) throw err;

        if (user) {
            user.nickname = req.body.nickname;
            user.save(function(err) {
                if (err) throw err;

                console.log('Modification du pseudo réussie');
                res.json({ success: true, message: 'Modification du pseudo réussie'});
            });
            
        }else if (!user){
            res.json({ success: false, message: 'L\'utilisateur n\'existe pas' });
        }
    });
});

// Param : 
router.post('/getNickname', function(req,res){
    User.findOne({
        email: req.body.email
    }, function(err, user){
         res.json(user.nickname);
    });
});            

router.post('/getPetUnlock', function(req,res){
   User.findOne({
        email: req.body.email
    }, function(err, user){
         res.json(user.petUnlock);
    });
}); 

router.post('/petUnlock', function(req,res){
   User.findOne({
        email: req.body.email
    }, function(err, user){

        let tab = [];
        let i = 0;
        // dirty mongoose bug on update array without index
        user.petUnlock.forEach(function(result){

            if(i==req.body.petNumber){
                tab.push(1); 
            }else{
                tab.push(result);
            }
            i++;
        });
        user.petUnlock = tab;
        user.save();
        //res.json({ success: true, message: 'Pet débloqué'});
        res.json(user);
    });
});

// Params : email:String | pet:Number
router.post('/setPet', function(req,res){
    User.findOne({
        email: req.body.email
    }, function(err, user){

        if (err) throw err;

        if (user) {
            user.pet = req.body.pet;
            user.save(function(err) {
                if (err) throw err;

                console.log('Modification du pet réussie');
                res.json({ success: true, message: 'Modification du pet réussie'});
            });
            
        }else if (!user){
            res.json({ success: false, message: 'L\'utilisateur n\'existe pas' });
        }
    });
});

router.post('/setDistance', function(req,res){
    User.findOne({
        email: req.body.email
    }, function(err, user){

        if (err) throw err;

        if (user) {
            user.distance = req.body.distance;
            user.save(function(err) {
                if (err) throw err;

                console.log('Modification du distance réussie');
                res.json({ success: true, message: 'Modification du distance réussie'});
            });
            
        }else if (!user){
            res.json({ success: false, message: 'L\'utilisateur n\'existe pas' });
        }
    });
});

router.post('/getPet',function(req,res){
    User.findOne({
        email: req.body.email
    }, function(err, user){
         res.json(user.pet);
    });
});

router.post('/getDistance',function(req,res){
    User.findOne({
        email: req.body.email
    }, function(err, user){
         res.json(user.distance);
    });
});

// Params : email | newemail
router.post('/email', function(req,res){
    User.findOne({
        email: req.body.email
    }, function(err, user){

        if (err) throw err;

        if (user) {
            User.findOne({
                email: req.body.newemail
            }, function(err, userCheck){
                if (!userCheck){

                    if (err) throw err;

                    user.email = req.body.newemail;
                    user.save(function  (err){
                        if (err) throw err;
                        console.log('Modification de l\'email réussie');
                        res.json({ success: true, message: 'Modification de l\'email réussie'});
                    });
                }else if(userCheck){
                    res.json({ success: false, message: 'Email déjà utilisé' });
                }
            });
            
        }else if (!user){
            res.json({ success: false, message: 'L\'utilisateur n\'existe pas' });
        }
    });
});


// Param  skin : Number | hair : Number | shirt : Number
router.post('/setAvatar',function(req,res){
    User.findOne({
        email: req.body.email
    }, function(err, user){
        if(!user){
            res.json({ success: false, message: 'L\'utilisateur n\'existe pas'});
        }
        user.avatar = [req.body.skin,req.body.hair,req.body.shirt];
        user.save(function  (err){
            if (err) throw err;
            console.log('Modification de l\'avatar réussie');
            res.json({ success: true, message: 'Modification de l\'avatar réussie'});
        });
    });
});


router.post('/getAvatar',function(req,res){
    User.findOne({
        email: req.body.email
    }, function(err, user){
        if (user){
         res.json(user.avatar);
        }else{
            res.json({ success: false, message: 'L\'utilisateur n\'existe pas' });
        }
    });
});


router.get('/users', function(req, res) {
    User.find({},{password:0}, function(err, users) {
        res.json(users);
    });
});


router.post('/getScore',function(req,res){

    User.findOne({ email:req.body.email},function(err,user){
        if (user){
         res.json(user.score);   
        }else{
            res.json({ success: false, message: 'L\'utilisateur n\'existe pas' });
        }
        
    });
});

router.post('/setScore',function(req,res){

    User.findOne({ email:req.body.email},function(err,user){
        if (user){
            user.score = req.body.score;
            user.save(function  (err){
                res.json({ success: true, message: 'Nouveau score enregistré'});
            }); 
        }else{
            res.json({ success: false, message: 'L\'utilisateur n\'existe pas' });
        }
    });
});


module.exports = router;