let express = require('express');
let jwt     = require('jsonwebtoken'); // used to create, sign, and verify tokens

let router = express.Router();

let Question   = require('../models/mongo/question'); // get our mongoose
let config = require('../../config'); // get our config file
let Answer   = require('../models/mongo/answer');
let interestPoint   = require('../models/mongo/interestPoint');
let secret = config.secret;

router.get('/question', function(req, res) {
    Question.find({}, function(err, question) {
        res.json(question);
    });
});



router.post('/addquestionandanswer',function(req,res){

	let labelQuestion=req.body.labelQuestion;
	let valueQuestion=req.body.valueQuestion;
	let interestPointName=req.body.interestPointName;
	let answerContent=req.body.answerContent;
	interestPoint.findOne({name:interestPointName},function(req,interestPoint){
		let NewAnswer=new Answer({
			content:answerContent
		});
		NewAnswer.save(function(err) {
            if (err) throw err;
            console.log('Answer saved successfully');
          
         });

		let NewQuestion=new Question({
			label:labelQuestion,
			value:valueQuestion,
			interestPointId:interestPoint._id,
			answerId:NewAnswer._id

		});

		NewQuestion.save(function(err) {
            if (err) throw err;
            console.log('Question saved successfully');
            res.json({ success: true, message: 'Question success'});
         });


	});

});

module.exports = router;