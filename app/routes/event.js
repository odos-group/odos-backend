let express = require('express');
let router  = express.Router();
let fs      = require('fs');
let mongoose = require('mongoose');

let Event = require('../models/mongo/event');

router.post('/getbyidevent',function(req,res){
    if (!req.body || typeof req.body.eventId !== "string"){
        res.status(400).send("Invalid/absent eventId");
        return;
    }

    let eventId  = mongoose.Types.ObjectId(req.body.eventId);
    let query  = Event.findOne(
        {
            _id: eventId
        }
    );

    query.exec().then(results =>{
        res.send(results || {});
    });
});

// Param 
router.post('/addEvent',function(req,res){
    let event = req.body.event;
    let coords = req.body.coords;

    let newevent = new Event({
        description : event.description,
        value : 10,
        type : event.type,
        location : {
            type : "Point",
            coordinates: [coords.lat,coords.lng]
        }
    });
    newevent.save(function(err) {
        if (err){
            res.status(400).send("Saving error");
            console.log("eer")
        }
        res.status(200).send(newevent);
    });
});

router.post('/event',function(req,res){
    let coords = req.body.coords;
    if (!coords || typeof coords.lat === "undefined" || typeof coords.lng === "undefined"){
        res.status(400).send("Invalid coords");
        return;
    }
    //todo rajouter tests de l'objet
    let query = Event.find(
        {
            location: //find a location near to the client ( the distance is a parameter)
            { $near:
                {
                    $geometry: {
                        type: "Point",
                        coordinates: [coords.lat, coords.lng ]
                    },
                    $maxDistance: 500
                }
            }
        }
    );
    query.exec()
        .then(results => {
            res.send(results);
        })
        .catch(function () {
            res.status(400).send("event search failed");
        });
});


module.exports = router;