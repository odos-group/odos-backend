let express         = require('express');
let tokenMiddleWare = require('../middlewares/tokenCheckMiddleware');

let router          = express.Router();

let imageHelper = require('../functions/imageHelper');

router.get('/checked', tokenMiddleWare.checkToken, function(req, res) {
    res.send('Hello checked!');
});

router.get('/unchecked', function(req, res) {
    res.send('Hello unchecked!');
});

router.post('/uploadImage', function(req, res) {
    if (req.body.image){
        let result = imageHelper.uploadImg(req.body.image);
        if (result){
            result.then(response => {
                if (response && response.success && response.status === 200 && response.data && response.data.link){
                    res.send(response.data.link);
                }
            })
            .catch(function () {
                res.status(400).send("Upload failed");
            });
        } else {
            res.status(400).send("Upload failed");
        }
    } else {
        res.status(400).send("No image");
    }
});

module.exports = router;