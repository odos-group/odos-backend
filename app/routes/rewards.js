let express = require('express');
let router  = express.Router();
let fs      = require('fs');
let mongoose = require('mongoose');

let Reward = require('../models/mongo/reward');
let Partner=require('../models/mongo/partner');

router.get('/rewards', function(req, res) {
    Reward.find({}, function(err, reward) {
        res.json(reward);
    });
});


router.post('/addreward',function(req,res){

    //res.json("loik");
	
	Partner.findOne({
        name: req.body.name
    }, function(err, partner) {

        if (err) throw err;

        if (partner) {
            // create a new success
            let NewReward = new Reward({
                description: req.body.description,
                price: req.body.price,
                partnerId: partner._id
            });
            // Save it in the database
            NewReward.save(function(err) {
                if (err) throw err;

                console.log('reward saved successfully');
                res.json({ success: true, message: 'add reward'});
            });

        } else if (!partner) {

            res.json({ success: false, message: 'add reward failed. reward already exist' });
        }

    });




});


module.exports=router;