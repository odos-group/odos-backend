let express = require('express');
let router  = express.Router();
let fs      = require('fs');
let mongoose = require('mongoose');

let Position = require('../models/mongo/position');

router.post('/getbyidposition',function(req,res){
    if (!req.body || typeof req.body.positionId !== "string"){
        res.status(400).send("Invalid/absent positionId");
        return;
    }

    let positionId  = mongoose.Types.ObjectId(req.body.positionId);
    let query  = Position.findOne(
        {
            _id: positionId
        }
    );

    query.exec().then(results =>{
        res.send(results || {});
    });
});


router.post('/addposition',function(req,res){
    let position = req.body.position;
    let coords = req.body.coords;

    let newposition = new Position({
        description : position.description,
        value : position.value,
        type : position.type,
        location : {
            type : "Point",
            coordinates: [coords.lat,coords.lng]
        }
    });
    newposition.save(function(err) {
        if (err){
            res.status(400).send("Saving error");
            console.log("eer")
        }
        console.log('position saved successfully');
        res.status(200).send(newposition);
    });
});

router.post('/position',function(req,res){
    let coords = req.body.coords;
    if (!coords || typeof coords.lat === "undefined" || typeof coords.lng === "undefined"){
        res.status(400).send("Invalid coords");
        return;
    }
    //todo rajouter tests de l'objet
    let query = Position.find(
        {
            location: //find a location near to the client ( the distance is a parameter)
            { $near:
                {
                    $geometry: {
                        type: "Point",
                        coordinates: [coords.lat, coords.lng ]
                    },
                    $maxDistance: 500
                }
            }
        }
    );
    query.exec()
        .then(results => {
            res.send(results);
        })
        .catch(function () {
            res.status(400).send("position search failed");
        });
});


module.exports = router;