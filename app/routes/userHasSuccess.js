let express = require('express');
let jwt     = require('jsonwebtoken'); // used to create, sign, and verify tokens

let router = express.Router();

let UserHasSuccess   = require('../models/mongo/userHasSuccess'); // get our mongoose
let config = require('../../config'); // get our config file
let User   = require('../models/mongo/user');
let Success=require('../models/mongo/success');

let secret = config.secret;

//Retourner la liste des success 
router.get('/userHasSuccess', function(req, res) {
    UserHasSuccess.find({}, function(err, usersHasSuccess) {
        res.json(usersHasSuccess);
    });
});


//Ajouter un success à un utilisateur
router.post('/addSuccessToUser',function(req,res){
	let userEmail=req.body.userEmail;
	let successName=req.body.successName;
	//res.json(userEmail)
	User.findOne({email:userEmail},function(req,user){

		

		Success.findOne({name:successName},function(req,success){
			let NewUserHasSuccess = new UserHasSuccess({
                userId: user.id,
                successId:success.id        
            });
            NewUserHasSuccess.save(function(err) {
                if (err) throw err;

                console.log('addSuccessToUser saved successfully');
                res.json({ success: true, message: 'add success'});
            });
		});
	
});
});


//Retourne la liste des success d'un utilisateur
router.post('/userSuccess',function(req,res){
	let userEmail=req.body.userEmail;
	User.findOne({email:userEmail},function(req,user){
        console.log(user.id);

		let query=UserHasSuccess.find({userId:user.id});
		query.exec()
        .then(results => {
            let idSuccess=[];
            results.forEach(function(result){
                idSuccess.push(result.successId);
            });
            let querySuccess = Success.find(
                {
                    _id:{
                        $in : idSuccess
                    }
                }
            );
            querySuccess.exec()
            .then(results =>{
                res.send(results);
            })
            .catch(function () {
                res.status(400).send("success search failed");
            });
            
            
        })
        .catch(function (err) {
            if (err) throw err;
            res.status(400).send("userSuccess search failed");
        });
	});
});






module.exports = router;