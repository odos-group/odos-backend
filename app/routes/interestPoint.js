let express = require('express');
let router  = express.Router();
let fs      = require('fs');
let mongoose = require('mongoose');

let InterestPoint = require('../models/mongo/interestPoint');

router.get('/allinterestpoint', function(req, res) {
    InterestPoint.find({}, function(err, interestPoint) {
        res.json(interestPoint);
    });
});





router.post('/interestPoint/test',function(req,res){
    if (!req.body || typeof req.body.interestPointId !== "string"){
        res.status(400).send("Invalid/absent interestPointId");
        return;
    }

    let interestPointId  = mongoose.Types.ObjectId(req.body.interestPointId);
    let query  = InterestPoint.findOne(
        {
            _id: interestPointId
        }
    );

    query.exec().then(results =>{
        res.send(results || {});
    });
});

router.post('/addinterestPoint',function(req,res){
    let interestPoint = req.body.interestPoint;
    let coords = req.body.coords;

    let newinterestPoint = new InterestPoint({
        name : interestPoint.name,
        description : interestPoint.description,
        value : interestPoint.value,
        //pictureURL : interestPoint.pictureURL,
        //websiteURL : interestPoint.websiteURL,
        location : {
            type : "Point",
            coordinates: [coords.lat,coords.lng]
        }
    });
    newinterestPoint.save(function(err) {
        if (err){
            res.status(400).send("Saving error");
            console.log("eer")
        }
        console.log('interestPoint saved successfully');
        res.status(200).send(newinterestPoint);
    });
});

router.post('/interestPoint',function(req,res){
    let coords = req.body.coords;
    if (!coords || typeof coords.lat === "undefined" || typeof coords.lng === "undefined"){
        res.status(400).send("Invalid coords");
        return;
    }
    //todo rajouter tests de l'objet

    let query = InterestPoint.find(
        {
            location: //find a location near to the client ( the distance is a parameter)
            { $near:
                {
                    $geometry: {
                        type: "Point",
                        coordinates: [coords.lat, coords.lng ]
                    },
                    $maxDistance: 500
                }
            }
        }
    );
    query.exec()
        .then(results => {
            res.send(results);
        })
        .catch(function () {
            res.status(400).send("interestPoint search failed");
        });
});


module.exports = router;