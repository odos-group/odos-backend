let express = require('express');
let router  = express.Router();
let fs      = require('fs');
let mongoose = require('mongoose');

let Partner = require('../models/mongo/partner');
let Reward = require('../models/mongo/reward');

router.get('/partners', function(req, res) {
    Partner.find({}, function(err, partner) {
        res.json(partner);
    });
});


router.post('/addpartner',function(req,res){
	
	let partner = req.body.partner;
    let coords = req.body.coords;

    let newPartner = new Partner({
        name : partner.name,
        website : partner.website,
        location : {
            type : "Point",
            coordinates: [coords.lat,coords.lng]
        }
    });
    newPartner.save(function(err) {
        if (err){
            res.status(400).send("Saving error");
            console.log("eer")
        }
        console.log('partner saved successfully');
        res.status(200).send(newPartner);
    });



});
router.post('/deletepartner',function(req,res){

    let namePartner=req.body.name;
    Partner.findOne({
        name:namePartner
    },function(err,partner){

        Reward.deleteMany({partnerId:partner._id},function(err,reward){
            if (err) throw err;
            
        });
        Partner.deleteOne({
        name: namePartner
    }, function(err, partner) {
        if (err) throw err;

        if (partner) {
            res.json({ success: true, message: 'delete partner successed' });

        } else if (partner) {

            res.json({ success: false, message: 'delete partner failed' });
        }

       

    });

    });
   
});


module.exports=router;