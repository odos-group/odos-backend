const clarifai = require('clarifai');
const app = new clarifai.App({
    apiKey: 'fe716ad276e844f19e8e27d472fc3a66'
});
const rp = require('request-promise');

module.exports = {
    uploadImg: function(imageContent) {
        if (imageContent){
            let cleanImage = imageContent.replace("data:image/jpeg;base64,", ""); //remove this header
            let options = {
                method: 'POST',
                uri: 'https://api.imgur.com/3/image',
                body: { image: cleanImage, type: "base64" },
                headers: {
                    'Authorization': 'Client-ID bbce58d43cc0f43'
                },
                json: true // Automatically stringifies the body to JSON
            };
            return rp(options);
        } else {
            return false;
        }
    },
    reconizeImg: function (imageUrl){
        if (imageUrl){
            return app.models.predict(Clarifai.GENERAL_MODEL, imageUrl);
        } else {
            return false;
        }
    }
};