let config = require('../../config'); // get our config file
let jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens

let secret = config.secret;

module.exports = {
    checkToken: function(req, res, next) {
        let token = "";
        // check header or url parameters or post parameters for token
        if (req.headers['authorization'] || req.headers['Authorization']){
            token = (req.headers['authorization'] ? req.headers['authorization'] : req.headers['Authorization']).replace("Bearer ", "");
        } else {
            token = req.body.token || req.param('token') || req.headers['x-access-token'];
        }

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, secret, function (err, decoded) {
                if (err) {
                    return res.json({success: false, message: 'Failed to authenticate token.'});
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });
        }
    }
};